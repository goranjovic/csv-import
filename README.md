# csv-importer

Imports the provided csv file into a dynamically generated sqlite database

Usage format:

```
./csv-importer filename

```

Example:

```
./csv-importer ./data/cities.csv

```

