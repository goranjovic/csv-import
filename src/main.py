import sys
import csv
import sqlite3
import os

if len(sys.argv) != 2:
    print("Usage: ./csv-import filename")
    sys.exit(-1)

filename = sys.argv[1]

rows = []

with open(filename, 'rb') as file:
    reader = csv.reader(file, skipinitialspace=True, delimiter=',', quotechar='"')
    headers = reader.next()
    for row in reader:
        rows.append(row)

db_name = "csv_import.db"
is_initialized = os.path.exists(db_name)
conn = sqlite3.connect(db_name)

# This is an illustrative example where table structure is dynamically generated based on detected columns in
# the provided csv file. While that approach is interesting for demos like this one, it also has some disadvantages:
# - csv file provided needs to be trusted not to insert malicious sql in the column headers (or even just honest
#   csv column names that just happen to be invalid sql column names)
# - alternatively, this could be alleviated with csv data sanitization, but the approach I'd likely use in a real
#   world scenario would be to just hardcode the csv structure.
#
# Which choices are right depends on the context in which this script would be run, e.g. is it a one-off script to
# import some backups and never be used again, or a mission critical data import that runs on cron every 15 min, etc.

schema_ddl = "create table if not exists result (id integer primary key autoincrement not null"
for header in headers:
    schema_ddl += ", " + header + " text"
schema_ddl += ");"
conn.execute(schema_ddl)

insert_query = 'insert into result (' + ', '.join(headers) + ') values (' + ', '.join(['?'] * len(headers)) + ');'
for row in rows:
    if len(row) == len(headers):
        conn.execute(insert_query, row)
conn.commit()

c = conn.cursor()
c.execute("select count(*) from result")
count = c.fetchone()[0]

print("Inserted %d rows " % len(rows))
print("Total rows after insertion: %d" % count)

